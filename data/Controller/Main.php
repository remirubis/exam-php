<?php

namespace Controller;

use Model;

class Main
{
  public function __construct()
  {
    // Test item
    $cornFlackes = new Model\Item("Corn flakes", 500, 2000);
    $this->format($cornFlackes->label());
    $this->format($cornFlackes->cost());
    $this->format($cornFlackes->toString());
    $this->format($cornFlackes->taxRatePerTenThousand());
    
    $chocapic = new Model\Item("Chocapic", 403, 9000);
    $this->format($chocapic->label());
    $this->format($chocapic->cost());
    $this->format($chocapic->toString());
    $this->format($chocapic->taxRatePerTenThousand());
    
    // Test shopping cart
    $shoppingCart = new Model\ShoppingCart();
    $shoppingCart->addItem($cornFlackes);
    $shoppingCart->addItem($cornFlackes);
    $shoppingCart->addItem($chocapic);
    $shoppingCart->removeItem(0);
    $shoppingCart->toString();

    // Test fresh item
    $chicken = new Model\FreshItem("Chicken", 4023, 6000, "2022-10-12");
    $this->format($chicken->label());
    $this->format($chicken->cost());
    $this->format($chicken->getBestBeforeDate());
    $this->format($chicken->toString());
    $this->format($chicken->taxRatePerTenThousand());

    // Ticket
    $invoice = new Model\Invoice();
    $walles = new Model\Ticket("RGBY17032012 - Walles-France", 9000);
    $this->format($walles->label());
    $this->format($walles->cost());
    $this->format($walles->taxRatePerTenThousand());
    $rolling = new Model\Ticket("MUSI20120612 - RollingStones", 1200);
    $rolling = new Model\Ticket("SALUT - Remi", 1900);
    $rolling = new Model\Ticket("dssjdsjdnjs - BEB", 20000);
    $invoice->add($walles);
    $invoice->add($rolling);

    $this->format($invoice->totalAmount());
    $this->format($invoice->totalTax());
  }
  
  public function format($data) : void
  {
    echo '<pre>' . var_export($data, true) . '</pre>';
  }

}
