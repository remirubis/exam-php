<?php

namespace Model;

use Exception;


class ShoppingCart
{
  private string $id;
  private string $name;
  private int $price;
  private array $shoppingCart;
  private int $cartWeight = 0;


  public function __construct()
  {
    $this->id = uniqid();
    $this->shoppingCart = array();
    $this->cartWeight = 0;
  }

  public function getId() : string
  {
    return $this->id;
  }
  
  public function addItem(Item $item) : bool
  {
    $newWeight = $item->getWeight() + $this->cartWeight;
    try {
      if ($newWeight > 10000) throw new Exception();
      $this->shoppingCart[] =  $item;
      $this->cartWeight = $newWeight;
    } catch (Exception $th) {
      echo "The item " . $item->label() . " is too heavy to be added to the cart, (Total with this article: " . $newWeight / 1000 . "kg, excepted: <= 10kg) <br />";
      return false;
    }
    return true;
  }

  public function removeItem(int $key): bool
  {
    if (empty($this->shoppingCart[$key])) {
      return false;
    }

    array_splice($this->shoppingCart, $key, 1);

    return true;
  }

  public function itemCount() : int
  {
    return count($this->shoppingCart);
  }

  // I loop through all the items and add the price of each item to the cumulative total variable
  public function totalPrice() : int
  {
    $total = 0;
    foreach ($this->shoppingCart as $item)
    {
      $total += $item->getPrice();
    }
    return $total;
  }

  public function toString() : void
  {
    echo "ID: " . $this->id . "<br />";
    echo "Number of items: " . $this->itemCount() . "<br />";
    echo "List of items: <br />";
    foreach ($this->shoppingCart as $item)
    {
      echo "- " . $item->toString() . "<br />";
    }
  }
}


