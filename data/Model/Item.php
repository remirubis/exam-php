<?php

namespace Model;

class Item extends Payable
{
  private int $weight;

  public function __construct(string $label, float $price, int $weight)
  {
    parent::__construct($label, $price, 10);
    $this->weight = $weight;
  }

  public function getWeight() : int
  {
    return $this->weight;
  }

  public function formatPrice() : string
  {
    return number_format(parent::cost() / 100, 2, '.', '');
  }
  

  public function toString() : string
  {
    return parent::label() . " (weight: " . $this->weight / 1000 . "kg): " . $this->formatPrice() . " €";
  }
}
