<?php

namespace Model;

class Invoice
{
  private array $payableList;

  public function __construct()
  {
    $this->payableList = [];
  }

  public function add(Payable $payable)
  {
    $this->payableList[] = $payable;
  }

  public function totalAmount() : int
  {
    $total = 0;
    foreach ($this->payableList as $payable) {
      $total += $payable->cost();
    }
    return $total;
  }

  public function totalTax() : int
  {
    $total = 0;
    foreach ($this->payableList as $payable) {
      $total += $payable->taxRatePerTenThousand();
    }
    return $total;
  }

}
