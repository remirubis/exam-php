<?php

namespace Model;

use Exception;

class FreshItem extends Item
{
  private string $bestBeforeDate;

  public function __construct(string $name, int $price, int $weight, string $bestBeforeDate)
  {
    try {
      if (!$this->checkDateFormat($bestBeforeDate)) throw new Exception("Wrong date format, please use: year-month-day");

      parent::__construct($name, $price, $weight);
      $this->bestBeforeDate = $bestBeforeDate;
    } catch (Exception $err) {
      echo $err->getMessage();
    }
  }

  public function checkDateFormat(string $date) : bool
  {
    return preg_match("/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/", $date);
  } 

  public function getBestBeforeDate() : string
  {
    return $this->bestBeforeDate;
  }

  public function taxRatePerTenThousand(): int
  {
    $step = parent::getWeight() / 1000;
    $taxReduction = $step > 1 ? $step * 0.1 : 0;
    $tax = ((10 - $taxReduction) * parent::cost()) / 100;
    return round($tax);
  }

  public function toString() : string
  {
    return "[" . $this->bestBeforeDate . "] " . parent::toString();
  }
}
