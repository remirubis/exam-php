<?php

namespace Model;

class Payable
{
  private string $label;
  private int $price;
  private float $taxRate;

  public function __construct(string $label, int $price, float $taxRate)
  {
    $this->label = $label;
    $this->price = $price;
    $this->taxRate = $taxRate;
  }

  public function label() : string
  {
    return $this->label;
  }

  public function cost() : int
  {
    return $this->price;
  }

  public function taxRatePerTenThousand(): int
  {
    $tax = ($this->taxRate * $this->price) / 100;
    return round($tax);
  }
}
