<?php

namespace Model;

class Ticket extends Payable
{
  public function __construct(string $reference, int $price)
  {
    parent::__construct($reference, $price, 25);
  }
}
